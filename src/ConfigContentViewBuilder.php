<?php

namespace Drupal\config_content_storage;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;

/**
 * Provides a view controller for a config content entity type.
 */
class ConfigContentViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getBuildDefaults(EntityInterface $entity, $view_mode) {
    $build = parent::getBuildDefaults($entity, $view_mode);
    // The config content has no entity template itself.
    unset($build['#theme']);
    return $build;
  }

}

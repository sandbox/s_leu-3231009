<?php

namespace Drupal\config_content_storage;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the config content entity type.
 */
class ConfigContentAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view config content');

      case 'update':
        return AccessResult::allowedIfHasPermissions($account, ['edit config content', 'administer config content'], 'OR');

      case 'delete':
        return AccessResult::allowedIfHasPermissions($account, ['delete config content', 'administer config content'], 'OR');

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, ['create config content', 'administer config content'], 'OR');
  }

}

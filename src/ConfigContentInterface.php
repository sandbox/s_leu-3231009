<?php

namespace Drupal\config_content_storage;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a config content entity type.
 */
interface ConfigContentInterface extends EntityPublishedInterface, ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the config content creation timestamp.
   *
   * @return int
   *   Creation timestamp of the config content.
   */
  public function getCreatedTime();

  /**
   * Sets the config content creation timestamp.
   *
   * @param int $timestamp
   *   The config content creation timestamp.
   *
   * @return \Drupal\config_content_storage\ConfigContentInterface
   *   The called config content entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the config content status.
   *
   * @return bool
   *   TRUE if the config content is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the config content status.
   *
   * @param bool $status
   *   TRUE to enable this config content, FALSE to disable.
   *
   * @return \Drupal\config_content_storage\ConfigContentInterface
   *   The called config content entity.
   */
  public function setStatus($status);

}

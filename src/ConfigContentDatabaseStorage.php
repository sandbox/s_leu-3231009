<?php

namespace Drupal\config_content_storage;

use Drupal\Core\Config\StorageInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\wse\WorkspaceIdDetector;

/**
 * Defines the Database storage.
 */
class ConfigContentDatabaseStorage implements StorageInterface {
  use DependencySerializationTrait;

  /**
   * @todo
   */
  const TABLE = 'config_content_field_data';

  /**
   * The decorated config.storage service.
   *
   * @var \Drupal\Core\Config\DatabaseStorage
   */
  protected $inner;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The workspace ID detector.
   *
   * @var \Drupal\wse\WorkspaceIdDetector
   */
  protected $workspaceIdDetector;

  /**
   * The storage collection.
   *
   * @var string
   */
  protected $collection = StorageInterface::DEFAULT_COLLECTION;

  /**
   * Constructs a new ConfigContentDatabaseStorage.
   *
   * @param \Drupal\Core\Config\StorageInterface $inner
   *   The decorated config.storage.active service
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param string $collection
   *   (optional) The collection to store configuration in. Defaults to the
   *   default collection.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(StorageInterface $inner, EntityTypeManagerInterface $entity_type_manager, WorkspaceIdDetector $workspace_id_detector, $collection = StorageInterface::DEFAULT_COLLECTION) {
    $this->inner = $inner;
    $this->entityTypeManager = $entity_type_manager;
    $this->workspaceIdDetector = $workspace_id_detector;
    $this->collection = $collection;
  }

  /**
   * {@inheritdoc}
   */
  public function exists($name) {
    if (!$this->configContentStorageIsActive()) {
      return $this->inner->exists($name);
    }

    try {
      // The DB query is faster than using an EQ as in the commented code below.
      // @todo use injected service.
      $exists = \Drupal::database()->queryRange('SELECT 1 FROM {' . \Drupal::database()->escapeTable(static::TABLE) . '} WHERE [collection] = :collection AND [name] = :name', 0, 1, [
        ':collection' => $this->collection,
        ':name' => $name,
      ])->fetchField();
//      $exists = $this->getConfigContentStorage()->loadByProperties([
//        'collection' => $this->collection,
//        'name' => $name,
//      ]);

      // Cover the case where this module is active, but existing config was not
      // yet saved into content entities.
      if (!$exists) {
        $exists = $this->inner->exists($name);
      }
      return $exists;
    }
    catch (\Exception $e) {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function read($name) {
    if (!$this->configContentStorageIsActive()) {
      return $this->inner->read($name);
    }

    $data = FALSE;
    try {
      $entity = $this->getConfigContentEntityByName($name);
      if ($entity && $entity->get('data')->value) {
        $data = $this->decode($entity->get('data')->value);
      }

      if (!$data) {
        $data = $this->inner->read($name);
      }
    }
    catch (\Exception $e) {
      // If we attempt a read without actually having the database or the table
      // available, just return FALSE so the caller can handle it.
    }
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function readMultiple(array $names) {
    if (!$this->configContentStorageIsActive()) {
      return $this->inner->readMultiple($names);
    }

    $list = [];
    try {
      $query = $this->getConfigContentStorage()
        ->getQuery()
        // @todo As discussed with Fabian: allRevisions must only be used if we are inside a workspace.
        ->allRevisions()
        ->condition('name', $names, 'IN')
        ->condition('workspace', $this->workspaceIdDetector->getActiveWorkspaceId());

      if ($this->getCollectionName()) {
        $query->condition('collection', $this->collection);
      }

      $result = $query->execute();
      $revisions = $this->getConfigContentStorage()->loadMultipleRevisions(array_keys($result));

//      $result = $this->getConfigContentStorage()
//        ->getQuery()
//        ->condition('name', $names, 'IN')
//        ->execute();
//      $revisions = $this->getConfigContentStorage()->loadMultiple(array_keys($result));

      foreach ($revisions as $revision) {
        $name = $revision->get('name')->value;
        $list[$name] = $this->decode(
          $revision->get('data')->value
        );
      }

      // At this point, the list contains only config which was already saved
      // inside a config content entity, but not all the other configs which may
      // have existed before enabling the module. Thus add the latter ones in.
      $list += $this->inner->readMultiple($names);
    }
    catch (\Exception $e) {
      // If we attempt a read without actually having the database or the table
      // available, just return an empty array so the caller can handle it.
    }
    return $list;
  }

  /**
   * {@inheritdoc}
   */
  public function write($name, array $data) {
    if (!$this->configContentStorageIsActive()) {
      return $this->inner->write($name, $data);
    }

    $serialized_data = $this->encode($data);
    try {
      $storage = $this->getConfigContentStorage();
      $entity = $this->getConfigContentEntityByName($name);
      if (!$entity) {
        $entity = $storage->create([
          'name' => $name,
          'collection' => $this->getCollectionName(),
          'data' => $serialized_data,
        ]);
      }
      else {
        $entity->set('name', $name);
        $entity->set('collection', $this->getCollectionName());
        $entity->set('data', $serialized_data);
      }
      $entity->save();
      return TRUE;
    }
    catch (\Exception $e) {
      // Some other failure that we can not recover from.
      throw $e;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function delete($name) {
    if (!$this->configContentStorageIsActive()) {
      return $this->inner->delete($name);
    }

    try {
      $entity = $this->getConfigContentEntityByName($name);
      $entity->delete();
      return TRUE;
    }
    catch (\Exception $exception) {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function rename($name, $new_name) {
    // @todo can this scenario (renaming while enabling the module) actually happen?
    if (!$this->configContentStorageIsActive()) {
      return $this->inner->rename($name, $new_name);
    }

    try {
      $entity = $this->getConfigContentEntityByName($name);
      $entity->set('name', $new_name)->save();
      return TRUE;
    }
    catch (\Exception $exception) {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function encode($data) {
    return serialize($data);
  }

  /**
   * Implements Drupal\Core\Config\StorageInterface::decode().
   *
   * @throws ErrorException
   *   The unserialize() call will trigger E_NOTICE if the string cannot
   *   be unserialized.
   */
  public function decode($raw) {
    return $this->inner->decode($raw);
  }

  /**
   * {@inheritdoc}
   */
  public function listAll($prefix = '') {
    if (!$this->configContentStorageIsActive()) {
      return $this->inner->listAll($prefix);
    }

    try {
      $query = \Drupal::database()->select(static::TABLE, 'c')
        ->fields('c', ['id'])
        ->condition('name', $prefix . '%', 'LIKE')
        ->orderBy('collection')->orderBy('name');
      if ($this->getCollectionName()) {
        $query->condition('collection', $this->collection, '=');
      }
      $result = $query->execute()->fetchCol();
//      $query->fields($this->table, ['name']);
//      $query->condition('collection', $this->collection, '=');
//      $query->condition('name', $prefix . '%', 'LIKE');
//      $query->orderBy('collection')->orderBy('name');
//      return $query->execute()->fetchCol();

//      $query = $this->getConfigContentStorage()->getQuery()
//        ->condition('name', $prefix, 'STARTS_WITH')
//        ->sort('collection')
//        ->sort('name');
//      if ($this->getCollectionName()) {
//        $query->condition('collection', $this->getCollectionName());
//      }
//      $result = $query->execute();

      $configs = [];
      if ($result) {
        $entities = $this->getConfigContentStorage()->loadMultipleRevisions($result);
        foreach ($entities as $entity) {
          $configs[] = $entity->get('name')->value;
        }
      }

      $configs += $this->inner->listAll($prefix);

      return $configs;
    }
    catch (\Exception $e) {
      return [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAll($prefix = '') {
    if (!$this->configContentStorageIsActive()) {
      return $this->inner->deleteAll($prefix);
    }

    try {
      $result = $this->getConfigContentStorage()->getQuery()
        ->condition('collection', $this->getCollectionName())
        ->condition('name', $prefix, 'STARTS_WITH')
        ->execute();

      if ($result) {
        $entities = $this->getConfigContentStorage()->loadMultiple($result);
        foreach ($entities as $entity) {
          $entity->delete();
        }
      }
      return TRUE;
    }
    catch (\Exception $e) {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createCollection($collection) {
    return new static(
      $this->inner,
      $this->entityTypeManager,
      $this->workspaceIdDetector,
      $collection
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCollectionName() {
    return $this->collection;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllCollectionNames() {
    if (!$this->configContentStorageIsActive()) {
      return $this->inner->getAllCollectionNames();
    }

    try {
      $result = $this->getConfigContentStorage()->getQuery()
        ->condition('collection', $this->getCollectionName(), '<>')
        ->sort('collection')
        ->execute();

      $collections = [];
      if ($result) {
        $entities = $this->getConfigContentStorage()->loadMultiple($result);
        foreach ($entities as $entity) {
          $collection = $entity->get('collection')->value;
          if (!in_array($collection, $collections)) {
            $collections[] = $collection;
          }
        }
      }

      $collections += $this->inner->getAllCollectionNames();
      return $collections;
    }
    catch (\Exception $e) {
      return [];
    }
  }

  /**
   * Loads a config content entity by name.
   *
   * @param string $name
   *   The name of the entity to be loaded.
   *
   * @return \Drupal\Core\Entity\EntityInterface|FALSE
   *   A loaded entity or false if none was found for given name.
   */
  protected function getConfigContentEntityByName($name) {
    $entities = $this->getConfigContentEntitiesByNames([$name]);
    return reset($entities);
  }

  /**
   * Loads a config content entity by name.
   *
   * @param array $names
   *   The name of the entity to be loaded.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   Loaded entities.
   */
  protected function getConfigContentEntitiesByNames(array $names) {
    return $this->getConfigContentStorage()->loadByProperties(['name' => $names]);
  }

  /**
   * @return bool
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function configContentStorageIsActive() {
    return (
      (bool) $this->entityTypeManager
        ->getDefinition('config_content', FALSE)
      && $this->workspaceIdDetector->getActiveWorkspaceId()
    );
  }

  /**
   * @return \Drupal\Core\Entity\ContentEntityStorageInterface
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getConfigContentStorage() {
    return $this->entityTypeManager->getStorage('config_content');
  }

}

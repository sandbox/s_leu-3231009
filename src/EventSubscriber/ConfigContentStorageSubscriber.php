<?php

namespace Drupal\config_content_storage\EventSubscriber;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\workspaces\Event\WorkspaceSafeFormEvent;
use Drupal\workspaces\WorkspaceEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Config Content Storage event subscriber.
 */
class ConfigContentStorageSubscriber implements EventSubscriberInterface {

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * Kernel request event handler.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   Response event.
   */
  public function onWorkspaceSafeForm(WorkspaceSafeFormEvent $event) {
    // @todo figure out how to do limit this to config entity forms only,
    //   possibly check the form's entity object and check if it's an instance of ConfigEntityInterface.
    $event->setSafe(TRUE);
    $event->stopPropagation();
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    if (class_exists(WorkspaceEvents::class)) {
      $events[WorkspaceEvents::WORKSPACE_SAFE_FORM][] = 'onWorkspaceSafeForm';
    }
    return $events;
  }

}

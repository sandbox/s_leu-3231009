<?php

namespace Drupal\config_content_storage\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the config content entity edit forms.
 */
class ConfigContentForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New config content %label has been created.', $message_arguments));
      $this->logger('config_content_storage')->notice('Created new config content %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The config content %label has been updated.', $message_arguments));
      $this->logger('config_content_storage')->notice('Updated new config content %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.config_content.canonical', ['config_content' => $entity->id()]);
  }

}
